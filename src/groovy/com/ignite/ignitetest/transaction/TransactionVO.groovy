package com.ignite.ignitetest.transaction

import com.ignite.ignitetest.bank.BankAccount
import com.ignite.ignitetest.bank.BankTransaction
import com.ignite.ignitetest.bank.TransactionDetail
import com.ignite.ignitetest.common.Enums.TransactionType

/**
 * Created by bharat on 14/4/16.
 */
class TransactionVO {


    String valueDate
    String payer
    String receiver
    String description
    String amount
    String accountBalance

    TransactionVO(){

    }

    TransactionVO(BankTransaction bankTransaction, TransactionDetail transactionDetail){
        this.valueDate = bankTransaction.valueDate.format("dd/MM/yyyy")
        this.payer = bankTransaction.payer.accountNumber+"-"+bankTransaction.payer.person.fullName()
        this.receiver = bankTransaction.receiver.accountNumber+"-"+bankTransaction.receiver.person.fullName()
        this.description = transactionDetail.description
        this.amount = bankTransaction.amount.toString()
        this.accountBalance = transactionDetail.balanceAfterTransaction.toString()
    }



}
