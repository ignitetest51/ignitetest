package com.ignite.ignitetest.transaction

import com.ignite.ignitetest.bank.BankAccount
import com.ignite.ignitetest.common.AppUtil
import com.ignite.ignitetest.user.Person
import grails.validation.Validateable


@Validateable
class TransferFundCO {

    String accountNumber
    String documentNumber
    String valueDate
    String beneficiaryName
    String beneficiaryAccount
    String amount
    String description
    String referenceNumber
    String userName


    static constraints = {
        accountNumber nullable: false, blank: false
        documentNumber nullable: false, blank: false
        valueDate nullable: false, blank: false
        beneficiaryName blank: false, nullable: false
        beneficiaryAccount blank: false, nullable: false, validator: {val, obj, errorObject ->
            BankAccount bankAccount = BankAccount.findByAccountNumber(obj.beneficiaryAccount)
            if (!bankAccount) {
                return errorObject.rejectValue("beneficiaryAccount", "transferFundCO.beneficiaryAccount.validator.error", "transferFundCO.beneficiaryAccount.validator.error")
            }
            if (obj.accountNumber == obj.beneficiaryAccount) {
                return errorObject.rejectValue("beneficiaryAccount", "you.cannnot.transfer.fund.on.your.account.error", "you.cannnot.transfer.fund.on.your.account.error")
            }
        }
        amount blank: false, nullable: false, validator: {val, obj ->
            BankAccount bankAccount = BankAccount.findByAccountNumber(obj.accountNumber)
            Double amount = Double.parseDouble(obj.amount)
            if (bankAccount.accountBalance < amount) {
                return ['transferFundCO.amount.custom.validator.error']
            }
        }
        description blank: false, nullable: false
        referenceNumber blank: true, nullable: true
    }

    TransferFundCO(){

    }

    TransferFundCO(BankAccount bankAccount, String accountNumber){
        this.amount = 100
        this.documentNumber = 10
        this.valueDate = new Date().format(AppUtil.DATE_FORMAT)
        this.accountNumber = accountNumber
        this.beneficiaryName = bankAccount.person.fullName()
        this.beneficiaryAccount = bankAccount.accountNumber
        this.description = "Cash gift"

    }

}
