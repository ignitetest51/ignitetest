package com.ignite.ignitetest.common

import groovy.util.logging.Log4j
import org.apache.commons.lang.RandomStringUtils

import java.text.DateFormat
import java.text.SimpleDateFormat

@Log4j
class AppUtil {

    public static final DATE_FORMAT = 'dd/MM/yyyy'
    public static final CLIENT_ROLE = 'CLIENT_ROLE'
    public static final TELLER_ROLE = 'TELLER_ROLE'
    public static final ESTONIA_CODE = 'EE'



    static boolean save(def object) {
        log.debug "save $object"
        if (object.validate() && !object.hasErrors() && object.save(flush: true, failOnError: true)) {
            return true
        } else
            log.error "save failed : " + object.errors
        return false
    }


    static boolean delete(def object) {
        log.debug "delete $object"
        if (object.save(flush: true)) {
            return true
        } else
            log.error "delete failed : " + object.errors
        return false
    }


    /**
     * Method for creating uniqueId
     *
     * @param word
     * A character to append to the start of if
     *
     * @param id
     * A id used to create unique id
     *
     **/
    static String getUniqueId(String word, Long id) {
        String userId = Long.toString(id)
        int idSize = 5
        int loopLimit = idSize - userId.length()
        for (int i = 0; i < loopLimit; i++) {
            userId = "0" + userId
        }
        return word ? word+userId : userId
    }


    /**
     * Method for creating date from string
     *
     * @param dateString
     * A date in string format
     *
     * @return Date
     * A Date object
     **/
    static Date getDateFromString(String dateString) {
        Date date = null
        DateFormat format = new SimpleDateFormat(DATE_FORMAT)
        if (dateString) {
            try {
                date = format?.parse(dateString)
            } catch (Exception e) {
            }
        }
        return date
    }

    static String getUniqueBankAccountID(){
       return ESTONIA_CODE+RandomStringUtils.randomNumeric(16)
    }

}
