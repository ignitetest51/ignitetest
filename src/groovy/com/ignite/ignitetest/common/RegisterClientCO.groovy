package com.ignite.ignitetest.common

import grails.validation.Validateable


@Validateable
class RegisterClientCO {

    String firstName
    String lastName
    String personalIdNumber
    String username


    static constraints = {
        firstName nullable: false, blank: false
        lastName nullable: false, blank: false
        username nullable: false, blank: false, email: true
        personalIdNumber blank: false, nullable: false
    }
}
