package com.ignite.ignitetest.common

import grails.validation.Validateable

/**
 * Created by bharat on 15/4/16.
 */

@Validateable
class PasswordResetCO {

    String newPassword
    String confirmPassword
    String token

    static constraints = {
        newPassword(blank: false)
        confirmPassword(blank: false, validator: { val, obj ->
            if (val != obj.newPassword) {
                return ['confirm.password.and.new.password.dont.match.message']
            }
        })
    }
}
