package com.ignite.ignitetest

import com.ignite.ignitetest.auth.Role
import com.ignite.ignitetest.auth.User
import com.ignite.ignitetest.auth.UserRole
import com.ignite.ignitetest.bank.BankAccount
import com.ignite.ignitetest.common.AppUtil
import com.ignite.ignitetest.user.Person
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(TellerService)
@Mock([BankAccount, Person, Role, UserRole])
class TellerServiceSpec extends Specification {

    def setup() {
        Person client = new Person(username: "johndoe@ignite.com", password: "ignite", firstName: "john", lastName: "doe", personalIdNumber: "242432567")
        client.enabled = true
        client.save()
    }

    def cleanup() {
    }


    def "test for creating bank account for client"(){
        setup:

        when:
        BankAccount bankAccount = service.createBankAccount(Person.first())

        then:
        bankAccount.person.id == Person.first().id
    }
}
