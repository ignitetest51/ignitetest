package com.ignite.ignitetest

import com.ignite.ignitetest.auth.Role
import com.ignite.ignitetest.auth.UserRole
import com.ignite.ignitetest.bank.BankAccount
import com.ignite.ignitetest.common.AppUtil
import com.ignite.ignitetest.user.Person
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(BankAccount)
@Mock([BankAccount, Person])
class BankAccountSpec extends Specification {

    def setup() {
        Person client = new Person(username: "johndoe@ignite.com", password: "ignite", firstName: "john", lastName: "doe", personalIdNumber: "242432567")
        client.enabled = true
        client.save()

        BankAccount clientBankAccount = new BankAccount(accountNumber: AppUtil.uniqueBankAccountID, accountBalance:100)
        clientBankAccount.person = client
        client.addToBankAccounts(clientBankAccount)
        clientBankAccount.save()
        client.save()
    }

    def cleanup() {
    }

    void "test balance of account after deposit"() {
        setup:
        BankAccount bankAccount = BankAccount.first()

        when:
        bankAccount.deposit(20.00)

        then:
        bankAccount.balance == 120.00

    }


    void "test balance of account after withdraw"() {
        setup:
        BankAccount bankAccount = BankAccount.first()

        when:
        bankAccount.withdraw(30.00)

        then:
        bankAccount.balance == 70.00

    }
}
