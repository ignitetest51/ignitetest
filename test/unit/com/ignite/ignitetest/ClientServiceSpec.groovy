package com.ignite.ignitetest

import com.ignite.ignitetest.auth.Role
import com.ignite.ignitetest.auth.User
import com.ignite.ignitetest.auth.UserRole
import com.ignite.ignitetest.bank.BankAccount
import com.ignite.ignitetest.common.AppUtil
import com.ignite.ignitetest.user.Person
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(ClientService)
@Mock([BankAccount, Person, Role, UserRole])
class ClientServiceSpec extends Specification {

    def setup() {
        Person client = new Person(username: "johndoe@ignite.com", password: "ignite", firstName: "john", lastName: "doe", personalIdNumber: "242432567")
        client.enabled = true
        client.save()

        BankAccount clientBankAccount = new BankAccount(accountNumber: AppUtil.uniqueBankAccountID)
        clientBankAccount.person = client
        client.addToBankAccounts(clientBankAccount)
        clientBankAccount.save()
        client.save()

        BankAccount clientSecondAccount = new BankAccount(accountNumber: AppUtil.uniqueBankAccountID)
        clientSecondAccount.person = client
        client.addToBankAccounts(clientSecondAccount)
        clientSecondAccount.save()
        client.save()
    }

    def cleanup() {
    }

    def "test for checking list of bank account"(){
        setup:
        Person person = Person.first()

        when:
        def accountList = service.getListOfAccounts((person.bankAccounts as List<BankAccount>), person)

        then:
        accountList.size() == 2
    }
}
