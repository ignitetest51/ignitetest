package com.ignite.ignitetest
import com.ignite.ignitetest.bank.BankAccount
import com.ignite.ignitetest.common.RegisterClientCO
import com.ignite.ignitetest.transaction.TransactionVO
import com.ignite.ignitetest.user.Person
import grails.test.spock.IntegrationSpec

class ClientServiceIntegrationTests extends IntegrationSpec{

    def tellerService
    def clientService


    def setup() {
        RegisterClientCO clientCO = new RegisterClientCO(firstName: "john", lastName: "doe", username: "johndoe@ignite.com", personalIdNumber: "123123")
        tellerService.createClient(clientCO)

    }

    def cleanup() {
    }


    def "Testing creating of client by teller"(){
        setup:

        when:
        Person person = Person.findByUsername("johndoe@ignite.com")

        then:
        person.lastName == "doe"
        person.firstName == "john"
        person.username == "johndoe@ignite.com"
    }

    def "Testing account balance after creating client"(){
        setup:

        when:
        Person person = Person.findByUsername("johndoe@ignite.com")
        BankAccount bankAccount = person.bankAccounts.first()

        then:
        bankAccount.balance == 100.00
    }

    def "Testing for getting the history of account"(){
        setup:

        when:
        Person person = Person.findByUsername("johndoe@ignite.com")
        BankAccount bankAccount = person.bankAccounts.first()
        List<TransactionVO> transactions = clientService.getTransactionHistoryList(bankAccount.accountNumber)
        TransactionVO firstTransaction = transactions.first()

        then:
        transactions.size() == 1
        firstTransaction.accountBalance.toDouble() == 100.00
        firstTransaction.amount.toDouble() == 100.00
    }


    def "test for checking list client"(){
        setup:
        Person person = Person.findByUsername("johndoe@ignite.com")

        Person client = new Person(username: "testuser@ignite.com", password: "ignite", firstName: "test", lastName: "user", personalIdNumber: "242432567")
        client.enabled = true
        client.save()

        def userList = []
        userList.add(client)
        userList.add(person)

        when:
        def clientList = tellerService.getListOfClients(userList)

        then:
        clientList.size() == 2
    }


}
