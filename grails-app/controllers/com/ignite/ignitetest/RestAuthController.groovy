package com.ignite.ignitetest

import com.ignite.ignitetest.auth.AuthenticationToken
import com.ignite.ignitetest.common.AppUtil
import com.ignite.ignitetest.common.PasswordResetCO
import com.ignite.ignitetest.user.Person
import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

@Secured('IS_AUTHENTICATED_ANONYMOUSLY')
class RestAuthController {


    def checkCurrentUser() {
        def result = [:]
        AuthenticationToken authToken = AuthenticationToken.findByToken(params.token)
        if (authToken) {
            result.result = g.message(code: "result.success.message")
            result.username = authToken.username
        } else {
            log.error(g.message(code: "something.went.wrong.message"))
            result.result = g.message(code: "result.error.message")
        }
        render result as JSON
    }


    def logoutUser() {
        def result = [:]
        AuthenticationToken authToken = AuthenticationToken.findByToken(params.token)
        if (authToken) {
            AppUtil.delete(authToken)
            result.message = g.message(code: "user.logout.message")
            result.result = g.message(code: "result.success.message")
        } else {
            log.error(g.message(code: "something.went.wrong.message"))
            result.message = g.message(code: "something.went.wrong.message")
            result.result = g.message(code: "result.error.message")
        }
        render result as JSON
    }

    def loginUser() {
        def result = [:]
        Person person = Person.findByUsername(params.username)
        if (person) {
            if(person.enabled){
                result.result = g.message(code: "result.success.message")
            }else {
                result.token = person.uniqueId
                result.message = g.message(code: "first.login.change.password")
                result.result = g.message(code: "result.info.message")
            }
        } else {
            log.error(g.message(code: "something.went.wrong.message"))
            result.result = g.message(code: "result.error.message")
        }
        render result as JSON
    }


    def forgotPassword = {PasswordResetCO passwordResetCO ->
        def result = [:]
        if(passwordResetCO.validate()){
            Person person = Person.findByUniqueId(passwordResetCO.token)
            person.password = passwordResetCO.newPassword
            person.enabled = true
            AppUtil.save(person)
            result.result = g.message(code: "result.success.message")
            result.message = g.message(code: "password.changed.successfully")
        }else {
            log.error(g.message(code: "confirm.password.and.new.password.dont.match.message"))
            result.result = g.message(code: "result.error.message")
            result.message = g.message(error: passwordResetCO.errors.allErrors.first())
        }
        render result as JSON
    }
}
