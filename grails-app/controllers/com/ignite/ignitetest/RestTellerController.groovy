package com.ignite.ignitetest

import com.ignite.ignitetest.auth.Role
import com.ignite.ignitetest.auth.User
import com.ignite.ignitetest.auth.UserRole
import com.ignite.ignitetest.bank.BankAccount
import com.ignite.ignitetest.common.AppUtil
import com.ignite.ignitetest.common.RegisterClientCO
import com.ignite.ignitetest.transaction.TransferFundCO
import com.ignite.ignitetest.user.Person
import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional

@Secured('IS_AUTHENTICATED_ANONYMOUSLY')
class RestTellerController {

    def tellerService

    def list() {
        def result = [:]
        List<User> userList = UserRole.createCriteria().list {
            eq("role", Role.findByAuthority(AppUtil.CLIENT_ROLE))
            projections {
                property("user")
            }
        }
        if (userList && !userList.empty) {
            result.result = g.message(code: "result.success.message")
            result.clients = tellerService.getListOfClients(userList)
        } else {
            log.error(g.message(code: "something.went.wrong.message"))
            result.result = g.message(code: "result.error.message")
            result.message = g.message(code: "something.went.wrong.message.for.client")
        }
        render result as JSON
    }




    def addClient = { RegisterClientCO clientCO ->
        def result = [:]
        if(clientCO.validate()){
            if(Person.countByUsername(clientCO.username)){
                result.message = g.message(code: "teller.client.exist", args: [clientCO.username])
            }else {
                tellerService.createClient(clientCO)
                result.result = g.message(code: "result.success.message")
                result.message = g.message(code: "teller.client.registration.successful", args: [clientCO.username])
            }
        }else {
            log.error(g.message(code: "something.went.wrong.message"))
            result.message = g.message(code: "something.went.wrong.message")
        }
        render result as JSON
    }




    def addAccount() {
        def result = [:]
        Person person = Person.findByUniqueId(params.userId)
        if(person){
            tellerService.createBankAccount(person)
            result.result = g.message(code: "result.success.message")
            result.message = g.message(code: "teller.adding.account.message", args: [person.fullName()])
        }else {
            log.error(g.message(code: "something.went.wrong.message"))
            result.message = g.message(code: "something.went.wrong.message")
        }
        render result as JSON
    }

}
