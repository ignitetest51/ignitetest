package com.ignite.ignitetest

import com.ignite.ignitetest.bank.BankAccount
import com.ignite.ignitetest.bank.BankTransaction
import com.ignite.ignitetest.bank.TransactionDetail
import com.ignite.ignitetest.transaction.TransactionVO
import com.ignite.ignitetest.transaction.TransferFundCO
import com.ignite.ignitetest.user.Person
import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

import javax.transaction.Transaction

@Secured('IS_AUTHENTICATED_ANONYMOUSLY')
class RestClientController {

    def transactionService
    def clientService


    def accountList() {
        def result = [:]
        Person person = Person.findByUsername(params.userName)
        List<BankAccount> accountList = person.bankAccounts as List<BankAccount>
        if (accountList && !accountList.empty) {
            result.result = g.message(code: "result.success.message")
            result.accounts = clientService.getListOfAccounts(accountList, person)
        } else {
            result.result = g.message(code: "result.error.message")
            result.message = g.message(code: "please.add.account")
        }
        render result as JSON
    }



    def getAccountNameList() {
        def result = [:]
        Person person = Person.findByUsername(params.userName)
        List<String> accountNumberList = BankAccount.createCriteria().list {
            eq("person", person)
            projections {
                property("accountNumber")
            }
        }
        result.accountNumbers = accountNumberList
        result.fullName = person.fullName()
        render result as JSON
    }



    def getAccountBalance() {
        String balance = "0.00"
        String accountNumber = params.accountNumber
        if(accountNumber){
            BankAccount bankAccount = BankAccount.findByAccountNumber(accountNumber)
            balance = bankAccount.accountBalance ?: "0.00"
        }
        render balance
    }



    def transferFund = {TransferFundCO transferFundCO ->
        def result = [:]
        if(transferFundCO.validate()){
            if(params.confirm == "true"){
                transactionService.createTransactionOnTransferFunds(transferFundCO)
                result.result = g.message(code: "result.success.message")
                result.message = g.message(code: "transfer.money.successful")
            }else {
                result.result = g.message(code: "result.success.message")
            }
        }else {
            log.error(g.message(code: "something.went.wrong.message"))
            result.result = g.message(code: "result.error.message")
            result.message = g.message(error: transferFundCO.errors.allErrors.first())
        }
        render result as JSON
    }



    def getAccountHistory() {
        def result = [:]
        if(params.accountNumber){
            result.transactions = clientService.getTransactionHistoryList(params.accountNumber)
            result.result = g.message(code: "result.success.message")
        }else {
            result.result = g.message(code: "result.error.message")
            result.message = g.message(code: "something.went.wrong.message")
        }
        render result as JSON
    }
}
