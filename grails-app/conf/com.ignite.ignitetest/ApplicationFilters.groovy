package com.ignite.ignitetest

import com.ignite.ignitetest.auth.User
import com.ignite.ignitetest.common.AppUtil
import org.codehaus.groovy.grails.web.json.JSONObject

class ApplicationFilters {

    def springSecurityService

    def filters = {
        all(controller: '*', action: '*') {
            before = {
                println "******** Params: ${new Date()} ********* --> ${params}"
            }
            after = { Map model ->

            }
            afterView = { Exception e ->

            }
        }

        recordRequestParameters(controller: 'restAuth|restClient|restTeller', action: '*') {
            before = {
                if(params.action != "checkCurrentUser"){
                    User user = springSecurityService.currentUser as User
                    RequestParamRecord paramRecord = new RequestParamRecord()
                    paramRecord.ipAddress = request.getHeader("X-Forwarded-For") ?: request.getRemoteAddr()
                    paramRecord.controllerName = params.controller
                    paramRecord.actionName = params.action ?: 'default'
                    paramRecord.userId = user?.id
                    params.remove('controller')
                    params.remove('action')
                    paramRecord.params = new JSONObject(params)
                    AppUtil.save(paramRecord)
                }
            }
            after = { Map model ->

            }
            afterView = { Exception e ->

            }
        }
    }
}