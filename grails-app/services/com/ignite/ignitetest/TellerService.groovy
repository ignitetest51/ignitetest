package com.ignite.ignitetest

import com.ignite.ignitetest.auth.Role
import com.ignite.ignitetest.auth.User
import com.ignite.ignitetest.auth.UserRole
import com.ignite.ignitetest.bank.BankAccount
import com.ignite.ignitetest.common.AppUtil
import com.ignite.ignitetest.common.RegisterClientCO
import com.ignite.ignitetest.transaction.TransferFundCO
import com.ignite.ignitetest.user.Person
import grails.transaction.Transactional
import org.apache.commons.lang.RandomStringUtils

@Transactional
class TellerService {

    def transactionService


    /**
     * Method for creating client and assigning client role
     *
     * @param RegisterClientCO
     * A RegisterClientCO class
     *
     * @return Person
     * A Person domain class object
     **/
    Person createClient(RegisterClientCO clientCO){
        Person person = new Person(clientCO)
        AppUtil.save(person)
        Role clientRole = Role.findByAuthority(AppUtil.CLIENT_ROLE)
        AppUtil.save(new UserRole(user: person, role: clientRole))
        BankAccount bankAccount = createBankAccount(person)
        TransferFundCO transferFundCO = new TransferFundCO(bankAccount, transactionService.adminBankAccount.accountNumber)
        transactionService.createTransactionForCashGift(transferFundCO)
        return person
    }


    /**
     * Method for creating bank account for client
     *
     * @param Person
     * A Person class object
     *
     * @return BankAccount
     * A BankAccount domain class object
     **/
    BankAccount createBankAccount(Person person){
        BankAccount bankAccount = new BankAccount()
        bankAccount.accountNumber = AppUtil.uniqueBankAccountID
        bankAccount.person = person
        person.addToBankAccounts(bankAccount)
        AppUtil.save(bankAccount)
        AppUtil.save(person)
        return bankAccount
    }


    /**
     * Method for getting the list of clients
     *
     * @return List
     * A List of clients
     **/
    def getListOfClients(List<User> userList){
        def clients = []
        List<Person> clientList = userList ? (userList.unique() as List<Person>) : []
        clientList?.each { client ->
            def clientMap = [:]
            clientMap.username = client.username
            clientMap.firstName = client.firstName
            clientMap.lastName = client.lastName
            clientMap.personalIdNumber = client.personalIdNumber
            clientMap.dateCreated = client.dateCreated
            clientMap.userId = client.uniqueId
            clients << clientMap
        }
        return clients
    }

}
