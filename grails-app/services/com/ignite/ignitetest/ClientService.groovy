package com.ignite.ignitetest

import com.ignite.ignitetest.bank.BankAccount
import com.ignite.ignitetest.bank.BankTransaction
import com.ignite.ignitetest.bank.TransactionDetail
import com.ignite.ignitetest.transaction.TransactionVO
import com.ignite.ignitetest.user.Person
import grails.transaction.Transactional

@Transactional
class ClientService {


    /**
     * Method for getting the list of bank accounts of user
     *
     * @param accountList
     * A list of account
     *
     *
     * @param Person
     * A domain class object
     *
     * @return List
     * A List of Bank acccount
     **/
    def getListOfAccounts(List<BankAccount> accountList, Person person){
        def accounts = []
        accountList.each { bankAccount ->
            def accountMap = [:]
            accountMap.accountBalance = bankAccount.accountBalance ?: 0.00
            accountMap.accountNumber = bankAccount.accountNumber
            accountMap.holderName = person.fullName()
            accountMap.personalIdNumber = person.personalIdNumber
            accounts << accountMap
        }
        return accounts
    }


    /**
     * Method for getting the list of bank accounts of user
     *
     * @param accountNumber
     * Account number of the bank account
     *
     * @return List
     * A List of Bank acccount
     **/
    def getTransactionHistoryList(String accountNumber){
        List<TransactionVO> transactions = []
        BankAccount bankAccount = BankAccount.findByAccountNumber(accountNumber)
        List<BankTransaction> bankTransactionList = BankTransaction.executeQuery("from BankTransaction " +
                "where payer_id = :payer_id or receiver_id in :receiver_id",
                [payer_id: bankAccount.id, receiver_id: bankAccount.id], [readOnly: true, cache: true])

        if(bankTransactionList){
            bankTransactionList = bankTransactionList.reverse()
            List<TransactionDetail> detailList = TransactionDetail.executeQuery("from TransactionDetail " +
                    "where bank_account_id = :bank_account_id and bank_transaction_id in :bank_transaction_id",
                    [bank_account_id: bankAccount.id, bank_transaction_id: bankTransactionList.id], [readOnly: true, cache: true])

            bankTransactionList?.each { BankTransaction bankTransaction ->
                TransactionDetail transactionDetail = detailList.find{it.bankTransaction.id == bankTransaction.id} as TransactionDetail
                TransactionVO transactionVO = new TransactionVO(bankTransaction, transactionDetail)
                transactions << transactionVO
            }
        }
        return transactions
    }
}
