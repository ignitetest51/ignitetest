package com.ignite.ignitetest

import com.ignite.ignitetest.auth.Role
import com.ignite.ignitetest.auth.User
import com.ignite.ignitetest.auth.UserRole
import com.ignite.ignitetest.bank.BankAccount
import com.ignite.ignitetest.common.AppUtil
import com.ignite.ignitetest.user.Person
import grails.transaction.Transactional
import org.apache.commons.lang.RandomStringUtils

@Transactional
class BootstrapService {


    def main() {
        if (Role.count == 0)
            createRoles()

        if (Person.count == 0)
            createPerson()

    }


    /**
     * Method for Bootstrapping roles
     *
     **/
    def createRoles() {
        Role clientRole = Role.findOrCreateByAuthority(AppUtil.CLIENT_ROLE)
        AppUtil.save(clientRole)


        Role tellerRole = Role.findOrCreateByAuthority(AppUtil.TELLER_ROLE)
        AppUtil.save(tellerRole)

    }


    /**
     * Method for Bootstrapping Users
     *
     **/
    def createPerson(){
        Role tellerRole = Role.findByAuthority(AppUtil.TELLER_ROLE)
        Role clientRole = Role.findByAuthority(AppUtil.CLIENT_ROLE)


        //One User who is teller
        Person teller = new Person(username: "teller@ignite.com", password: "ignite", firstName: "teller", lastName: "ignite")
        teller.enabled = true
        AppUtil.save(teller)

        UserRole.create(teller, tellerRole, true)


        //One User who is admin
        Person admin = new Person(username: "admin@ignite.com", password: "ignite", firstName: "ignite", lastName: "bank")
        admin.enabled = true
        AppUtil.save(admin)
        UserRole.create(admin, tellerRole, true)
        BankAccount tellerBankAccount = new BankAccount(accountNumber: AppUtil.uniqueBankAccountID)
        tellerBankAccount.person = admin
        admin.addToBankAccounts(tellerBankAccount)
        AppUtil.save(tellerBankAccount)
        AppUtil.save(admin)



        //One User who is client
        Person client = new Person(username: "client@ignite.com", password: "1234", personalIdNumber:26039242364, firstName: "client", lastName: "ignite")
        client.enabled = true
        client.save(flush: true)
        AppUtil.save(client)


        UserRole.create(client, clientRole, true)

        BankAccount bankAccount = new BankAccount(accountNumber: AppUtil.uniqueBankAccountID, accountBalance: 100)
        bankAccount.person = client
        client.addToBankAccounts(bankAccount)
        AppUtil.save(bankAccount)
        AppUtil.save(client)
    }
}
