package com.ignite.ignitetest

import com.ignite.ignitetest.bank.BankAccount
import com.ignite.ignitetest.bank.BankTransaction
import com.ignite.ignitetest.bank.TransactionDetail
import com.ignite.ignitetest.common.AppUtil
import com.ignite.ignitetest.common.Enums.TransactionType
import com.ignite.ignitetest.transaction.TransferFundCO
import grails.transaction.Transactional
import org.springframework.context.MessageSource

@Transactional
class TransactionDetailService {

    MessageSource messageSource



    /**
     * Method for creating transaction details
     * for payer and receiver for Bank Transaction
     *
     * @param TransferFundCO
     * A TransferFundCO virtual class
     *
     * @param BankTransaction
     * A BankTransaction domain class object
     *
     **/
    void createTransactionOnTransferFunds(TransferFundCO transferFundCO, BankTransaction bankTransaction){

        // Transaction details for the payer
        TransactionDetail detailForPayer  = new TransactionDetail()
        detailForPayer.transactionType = TransactionType.WITHDRAW
        detailForPayer.referenceNumber = transferFundCO.referenceNumber ?: bankTransaction.uniqueId
        detailForPayer.description = messageSource.getMessage("transactionType.WITHDRAW", [].toArray(), null)+" | "+transferFundCO.description
        detailForPayer.balanceAfterTransaction = bankTransaction.payer.accountBalance
        detailForPayer.bankAccount = bankTransaction.payer
        detailForPayer.bankTransaction = bankTransaction
        AppUtil.save(detailForPayer)

        // Transaction details for the receiver
        TransactionDetail detailForReceiver  = new TransactionDetail()
        detailForReceiver.transactionType = TransactionType.DEPOSIT
        detailForReceiver.referenceNumber = bankTransaction.uniqueId
        detailForReceiver.description = messageSource.getMessage("transactionType.DEPOSIT", [].toArray(), null)+" | "+messageSource.getMessage("received.funds.from.message", [bankTransaction.payer.accountNumber].toArray(), null)
        detailForReceiver.balanceAfterTransaction = bankTransaction.receiver.accountBalance
        detailForReceiver.bankAccount = bankTransaction.receiver
        detailForReceiver.bankTransaction = bankTransaction
        AppUtil.save(detailForReceiver)
    }


    /**
     * Method for creating transaction details
     * for receiver for Bank Transaction
     *
     * @param BankTransaction
     * A BankTransaction domain class object
     *
     **/
    void createTransactionOnCashGift(BankTransaction bankTransaction){

        // Transaction details for the receiver
        TransactionDetail detailForReceiver  = new TransactionDetail()
        detailForReceiver.transactionType = TransactionType.CASH_GIFT
        detailForReceiver.referenceNumber = bankTransaction.uniqueId
        detailForReceiver.description = messageSource.getMessage("transactionType.CASH_GIFT", [].toArray(), null)+" | "+messageSource.getMessage("received.cash.gift.message", [bankTransaction.payer.accountNumber].toArray(), null)
        detailForReceiver.balanceAfterTransaction = bankTransaction.receiver.accountBalance
        detailForReceiver.bankAccount = bankTransaction.receiver
        detailForReceiver.bankTransaction = bankTransaction
        AppUtil.save(detailForReceiver)
    }
}
