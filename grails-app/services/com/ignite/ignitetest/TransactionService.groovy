package com.ignite.ignitetest

import com.ignite.ignitetest.bank.BankAccount
import com.ignite.ignitetest.bank.BankTransaction
import com.ignite.ignitetest.common.AppUtil
import com.ignite.ignitetest.transaction.TransferFundCO
import com.ignite.ignitetest.user.Person
import grails.transaction.Transactional

@Transactional
class TransactionService {

    def transactionDetailService


    /**
     * Method for creating transaction for client
     * for fund transfer
     *
     * @param TransferFundCO
     * A TransferFundCO virtual class
     *
     **/
    void createTransactionOnTransferFunds(TransferFundCO transferFundCO){
        log.info("Creating fund transfer transactions.")
        BankTransaction bankTransaction = new BankTransaction(transferFundCO)
        AppUtil.save(bankTransaction)
        bankTransaction.payer.transfer(bankTransaction.receiver, bankTransaction.amount)
        transactionDetailService.createTransactionOnTransferFunds(transferFundCO, bankTransaction)
        log.info("Successfully Created fund transfer transactions.")
    }


    /**
     * Method for creating transaction for Gift cash For new client
     * for fund transfer
     *
     * @param TransferFundCO
     * A TransferFundCO virtual class
     *
     **/
    void createTransactionForCashGift(TransferFundCO transferFundCO){
        log.info("Creating fund transfer transactions.")
        BankTransaction bankTransaction = new BankTransaction(transferFundCO)
        AppUtil.save(bankTransaction)
        bankTransaction.receiver.deposit(bankTransaction.amount)
        transactionDetailService.createTransactionOnCashGift(bankTransaction)
        log.info("Successfully Created fund transfer transactions.")
    }

    BankAccount getAdminBankAccount(){
        Person person = Person.findByUsername("admin@ignite.com")
        return person.bankAccounts.first()
    }
}
