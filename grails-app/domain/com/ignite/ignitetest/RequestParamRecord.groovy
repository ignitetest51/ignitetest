package com.ignite.ignitetest

class RequestParamRecord {

    Date dateCreated
    Long userId
    String ipAddress
    String params
    String controllerName
    String actionName


    static mapping = {
        version false
        params type: 'text'

    }

    static constraints = {
        userId nullable: true
        params nullable: true
        ipAddress nullable: true, blank: false, maxSize: 39
    }

}
