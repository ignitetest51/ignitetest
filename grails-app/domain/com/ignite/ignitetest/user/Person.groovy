package com.ignite.ignitetest.user

import com.ignite.ignitetest.auth.User
import com.ignite.ignitetest.bank.BankAccount
import com.ignite.ignitetest.common.RegisterClientCO

class Person extends User {

    String firstName
    String lastName
    String personalIdNumber
    String uniqueId = UUID.randomUUID().toString()

    static hasMany = [bankAccounts: BankAccount]


    static constraints = {
        firstName blank: false, nullable: false
        lastName blank: false, nullable: false
        personalIdNumber blank: true, nullable: true
    }

    String fullName() {
        firstName + " " + lastName
    }

    Person(){

    }

    Person(RegisterClientCO clientCO){
        this.firstName = clientCO.firstName
        this.lastName = clientCO.lastName
        this.username = clientCO.username
        this.password = UUID.randomUUID().toString()
        this.personalIdNumber = clientCO.personalIdNumber
    }

}
