package com.ignite.ignitetest.bank

import com.ignite.ignitetest.common.AppUtil
import com.ignite.ignitetest.user.Person

class BankAccount {

    String accountNumber
    String uniqueId = UUID.randomUUID().toString()
    Double accountBalance

    Date dateCreated
    Date lastUpdated

    static belongsTo = [person: Person]



    static constraints = {
        accountBalance nullable: true
        accountNumber unique: true
    }


    /**
     * Method for making credit the money
     *
     * @param amount
     * A amount variable which need to be added
     **/
    public void deposit(double amount) {
        this.accountBalance ? (this.accountBalance += amount) : (this.accountBalance = amount)
        AppUtil.save(this)
    }


    /**
     * Method to get balance of account
     *
     * @return double
     * balance of the account
     **/
    public double getBalance() {
        return accountBalance;
    }


    /**
     * Method for transfering the money
     *
     * @param amount
     * A amount variable which need to be transfered
     *
     * @param BankAccount
     * A BankAccount domain class object
     **/
    public void transfer(BankAccount target, double amount) {
        this.withdraw(amount);
        target.deposit(amount);
    }


    /**
     * Method for making debit the money
     *
     * @param amount
     * A amount variable which need to be debit
     **/
    public void withdraw(double amount) {
        if(this.accountBalance){
            amount ? (this.accountBalance -= amount) : ""
        }
        AppUtil.save(this)
    }
}
