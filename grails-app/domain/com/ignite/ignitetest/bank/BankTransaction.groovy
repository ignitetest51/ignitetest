package com.ignite.ignitetest.bank

import com.ignite.ignitetest.common.AppUtil
import com.ignite.ignitetest.common.Enums.TransactionType
import com.ignite.ignitetest.transaction.TransferFundCO

class BankTransaction {

    String documentNo

    Date valueDate

    Double amount

    Date dateCreated
    Date lastUpdated

    BankAccount receiver
    BankAccount payer


    static constraints = {
        amount nullable: true
    }


    String getUniqueId() {
        AppUtil.getUniqueId("#", this?.id)
    }

    BankTransaction(){

    }

    BankTransaction(TransferFundCO transferFundCO){
        this.documentNo = transferFundCO.documentNumber
        this.valueDate = AppUtil.getDateFromString(transferFundCO.valueDate)
        this.amount = Double.parseDouble(transferFundCO.amount)
        this.receiver = BankAccount.findByAccountNumber(transferFundCO.beneficiaryAccount)
        this.payer = BankAccount.findByAccountNumber(transferFundCO.accountNumber)
    }
}
