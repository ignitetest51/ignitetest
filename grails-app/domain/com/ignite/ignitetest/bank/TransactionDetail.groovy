package com.ignite.ignitetest.bank

import com.ignite.ignitetest.common.Enums.TransactionType

class TransactionDetail {

    TransactionType transactionType

    String referenceNumber
    String description

    Double balanceAfterTransaction

    BankAccount bankAccount
    BankTransaction bankTransaction

    static constraints = {
        referenceNumber nullable: true
        balanceAfterTransaction nullable: true
    }

    static mapping = {
        description type: 'text'
    }
}
