
var authApp = angular.module('authApp', []);

authApp.controller('loginController', function ($scope, $location, $http, toastr, $rootScope, $httpParamSerializerJQLike, ClientService) {
    var baseUrl = $('#appUrl').val();
    $scope.$on(
        "$routeChangeSuccess",
        function handleRouteChangeEvent(event) {
            $.material.init();
        }
    );

    $scope.formData = {};

    $scope.loginUser = function (form) {
        $scope.$broadcast('show-errors-check-validity');
        if (form.$invalid) {
            return;
        }
        $http({
            url: baseUrl + "/restAuth/loginUser",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $httpParamSerializerJQLike(this.formData)
        }).success(function (response) {
                if (response.result == "SUCCESS") {
                    ClientService.loginPostCall($scope.formData.username, $scope.formData.password);
                }else if(response.result == "INFO"){
                    toastr.info(response.message);
                    $location.path("/forgotPassword/"+response.token);
                } else {
                    toastr.error("Sorry, we were not able to find a user with that username and password.");
                    $location.path("/login");
                }
            }).
            error(function(response) {
                toastr.error("Something went wrong. Please try again.");
            });
    };
});

authApp.controller('forgotPassword', function ($scope, $location, $http, toastr, TellerService, $routeParams, $httpParamSerializerJQLike) {
    var baseUrl = $('#appUrl').val();
    $scope.$on("$routeChangeSuccess",function handleRouteChangeEvent(event) {
            $.material.init();
        }
    );
    var token = $routeParams.token;
    $scope.formData = {token: token};
    if(!token){
        $location.path("/login");
    }
    $scope.forgotPassword = function (form) {
        $scope.$broadcast('show-errors-check-validity');
        if (form.$invalid) {
            return;
        }
        $http({
            url: baseUrl + "/restAuth/forgotPassword",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $httpParamSerializerJQLike(this.formData)
        }).success(function (response) {
                if (response.result == 'SUCCESS') {
                    toastr.success(response.message);
                    TellerService.handleWhenUserNotLoggedIn();
                    $location.path("/login");
                }else {
                    $location.path("/forgotPassword/"+token);
                    toastr.info(response.message);
                }
            });
    };
});


authApp.controller('logoutController', function ($scope, $location, $http, toastr, TellerService) {
    var baseUrl = $('#appUrl').val();
    $scope.$on(
        "$routeChangeSuccess",
        function handleRouteChangeEvent(event) {
            $.material.init();
        }
    );
    var authToken = localStorage.getItem("authToken");

    $scope.logout = function () {
        $http.get(baseUrl + "/restAuth/logoutUser", {
            params: {
                token: authToken
            }
        }).success(function (data) {
                if (data.result == 'SUCCESS') {
                    toastr.success(data.message);
                    TellerService.handleWhenUserNotLoggedIn();
                }else {
                    $location.path("/");
                    toastr.error(data.message);
                }
            });
    };
    $scope.logout();
});

