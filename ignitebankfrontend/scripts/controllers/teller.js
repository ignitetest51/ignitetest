
var tellerApp = angular.module('tellerApp', []);


tellerApp.controller('addClientController', function ($scope, $routeParams, $http, $httpParamSerializerJQLike, toastr, $location) {
    var baseUrl = $('#appUrl').val();
    $scope.formData = {};

    $scope.$on("$routeChangeSuccess", function handleRouteChangeEvent(event) {
            $.material.init();
        }
    );

    $scope.submitAddClientForm = function (form) {
        $scope.$broadcast('show-errors-check-validity');
        if (form.$invalid) {
            return;
        }
        $http({
            url: baseUrl + "/restTeller/addClient",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $httpParamSerializerJQLike(this.formData)
        }).success(function (response) {
                if (response.result == 'SUCCESS') {
                    toastr.success(response.message);
                    $location.path("/");
                } else {
                    toastr.error(response.message);
                    $location.path("/addClient");
                }
            });
    }
});


tellerApp.controller('addAccountController', function ($scope, $routeParams, $http, $httpParamSerializerJQLike, toastr, $location) {
    var baseUrl = $('#appUrl').val();
    $scope.formData = {userId: $routeParams.userId};

    $scope.$on("$routeChangeSuccess", function handleRouteChangeEvent(event) {
            $.material.init();
        }
    );

    $scope.submitAddAccountForm = function (form) {
        $scope.$broadcast('show-errors-check-validity');
        if (form.$invalid) {
            return;
        }
        $http({
            url: baseUrl + "/restTeller/addAccount",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $httpParamSerializerJQLike(this.formData)
        }).success(function (response) {
                if (response.result == 'SUCCESS') {
                    toastr.success(response.message);
                    $location.path("/");
                } else {
                    toastr.error(response.message);
                    $location.path("/addAccount");
                }
            });
    }
});


tellerApp.controller('listController', function ($scope, TellerService) {
    TellerService.loadClientList(function (response) {
        $scope.clientList = response;
    });

    $scope.$on("$routeChangeSuccess", function handleRouteChangeEvent(event) {
            $.material.init();
        }
    );
});


