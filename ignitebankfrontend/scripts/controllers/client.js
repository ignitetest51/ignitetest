
var clientApp = angular.module('clientApp', []);


clientApp.controller('welcomePage', function ($scope, $location, toastr) {
    var username = localStorage.getItem("username");
    $scope.$on("$routeChangeSuccess",function handleRouteChangeEvent(event) {
            $.material.init();
        }
    );

    $scope.setWelcomePage = function (welcomePage) {
        localStorage.setItem("welcomePage", welcomePage);
        toastr.success("Preferred welcome page selected.");
        $location.path(welcomePage);
    };
});


clientApp.controller('accountList', function ($scope, ClientService) {
    var username = localStorage.getItem("username");
    ClientService.loadAccountList(username, function (response) {
        $scope.accountList = response;
    });

    $scope.$on("$routeChangeSuccess",function handleRouteChangeEvent(event) {
            $.material.init();
        }
    );
});


clientApp.controller('fundTransferController', function ($scope, $rootScope, $routeParams, $http, $httpParamSerializerJQLike, toastr, $filter, $location, ClientService) {
    var baseUrl = $('#appUrl').val();
    var username = localStorage.getItem("username");
    $scope.formData = {username: username, valueDate: $filter("date")(Date.now(), 'dd/MM/yyyy')};
    $scope.fullName = "";
    $scope.accountNumbers = {};
    $scope.accountBalance = "0.00";

    $scope.$on("$routeChangeSuccess", function handleRouteChangeEvent(event) {
            $.material.init();
        }
    );

    ClientService.getAccountNameList(username, function (data) {
        $scope.accountNumbers = data.accountNumbers;
        $scope.formData.fullName = data.fullName;
    });

    $scope.updateAccountBalance = function (accountNumber, callback) {
        ClientService.getAccountBalance(accountNumber, function (data) {
            $scope.accountBalance = data;
        });
    };

    $scope.submitFundTransferForm = function (form) {
        $scope.$broadcast('show-errors-check-validity');
        if (form.$invalid) {
            return;
        }
        $http({
            url: baseUrl + "/restClient/transferFund",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $httpParamSerializerJQLike(this.formData)
        }).success(function (response) {
                if (response.result == 'SUCCESS') {
                    $rootScope.userDetail = $scope.formData;
                    $location.path("/confirmTransfer");
                } else {
                    toastr.error(response.message);
                    $location.path("/fundTransfer");
                }
            });
    }
});


clientApp.controller('confirmTransferController', function ($scope, $rootScope, $routeParams, $http, $httpParamSerializerJQLike, toastr, $location) {
    var baseUrl = $('#appUrl').val();
    var formData = $rootScope.userDetail;
    if(formData){
        formData.confirm  = 'true';
    }else{
        $location.path("/fundTransfer");
    }
    $scope.userDetail = formData;

    $scope.$on("$routeChangeSuccess", function handleRouteChangeEvent(event) {
            $.material.init();
        }
    );

    $scope.resetFundTransfer = function () {
        delete $rootScope.userDetail;
        $location.path("/fundTransfer");
    };

    $scope.confirmFundTransfer = function () {
        $http({
            url: baseUrl + "/restClient/transferFund",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $httpParamSerializerJQLike(formData)
        }).success(function (response) {
                if (response.result == 'SUCCESS') {
                    toastr.success(response.message);
                    $location.path("/");
                } else {
                    toastr.error(response.message);
                    $location.path("/fundTransfer");
                }
            });
    }
});



clientApp.controller('transferHistoryController', function ($scope, $routeParams, $http, $httpParamSerializerJQLike, toastr, $location, ClientService) {
    var baseUrl = $('#appUrl').val();
    var username = localStorage.getItem("username");
    $scope.accountNumbers = {};

    $scope.$on("$routeChangeSuccess", function handleRouteChangeEvent(event) {
            $.material.init();
        }
    );

    $scope.getAccountHistory = function (accountNumber, callback) {
        ClientService.getAccountHistory(accountNumber, function (data) {
            if (data.result == 'SUCCESS') {
                $scope.transactionList = data;
            } else {
                toastr.error(data.message);
            }
        });
    };

    ClientService.getAccountNameList(username, function (data) {
        $scope.accountNumbers = data.accountNumbers;
        $scope.fullName = data.fullName;
    });

});

