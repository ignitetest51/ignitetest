exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['tellerlogin.js'],
    framework: 'jasmine',
    capabilities: {
        browserName: 'firefox'
    }
};