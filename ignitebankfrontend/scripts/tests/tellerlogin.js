describe('teller login', function() {

    var errorMessage = element(by.id('toast-container')).element(by.css('.toast-error')).element(by.css('.toast-message'));
    var successMessage = element(by.id('toast-container')).element(by.css('.toast-success')).element(by.css('.toast-message'));

    beforeEach(function() {
        browser.get('http://localhost:63342/ignitetest/ignitebankfrontend/index.html#/login');
        browser.driver.manage().window().setSize(1500, 1000);

        var origFn = browser.driver.controlFlow().execute;

        browser.driver.controlFlow().execute = function() {
            var args = arguments;

            // queue 100ms wait
            origFn.call(browser.driver.controlFlow(), function() {
                return protractor.promise.delayed(150);
            });

            return origFn.apply(browser.driver.controlFlow(), args);
        };

    });

    it('should login teller account and validate email and create client.', function() {

        // login to teller account
        loginUser('teller@ignite.com', 'ignite');

        // redirect to add client view
        addClient();

        // try to create exist account to check the email validation
        saveClient('teller@ignite.com', 'test', 'user', '13131314');
        expect(errorMessage.getText()).toEqual('Client with email address teller@ignite.com is already registered.');

        // create new client with unique email address.
        var emailAddress = 'testuser+'+getRandomNumber()+'@ignite.com';
        saveClient(emailAddress, 'test', 'user', '13131314');
        expect(successMessage.getText()).toEqual('Client with email address '+emailAddress+' has been registered.');

    });


    // function for login
    function loginUser(userName, userPassword) {
        var username = element(by.model('formData.username'));
        var password = element(by.model('formData.password'));
        var loginButton = element(by.id('loginButton'));
        username.sendKeys(userName);
        password.sendKeys(userPassword);
        loginButton.click();
    }


    // function for redirecting to client view
    function addClient() {
        var addClientButton = element(by.id('addClient'));
        addClientButton.click();
    }



    // function for giving input to add client form feilds and saving client by teller.
    function saveClient(username, firstname, lastname, personalidnumber) {
        var userName = element(by.model('formData.username'));
        var firstName = element(by.model('formData.firstName'));
        var lastName = element(by.model('formData.lastName'));
        var personalIdNumber = element(by.model('formData.personalIdNumber'));
        var createClientButton = element(by.id('createClient'));
        userName.clear().sendKeys(username);
        firstName.clear().sendKeys(firstname);
        lastName.clear().sendKeys(lastname);
        personalIdNumber.clear().sendKeys(personalidnumber);
        createClientButton.click();
    }


    // function for generating random numbers.
    function getRandomNumber() {
        return Math.floor((Math.random() * 1000) + 1);
    }
});
