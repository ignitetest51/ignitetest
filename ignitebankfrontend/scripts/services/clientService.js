
var clientService = angular.module('clientService', []);

clientService.service('ClientService', function ($http, $rootScope, $location, toastr) {

    var baseUrl = $('#appUrl').val();

    this.loadAccountList = function (username, callback) {
        $http.get(baseUrl + "/restClient/accountList", {params: {userName: username}}).success(function (data) {
            callback(data);
        });
    };

    this.getAccountNameList = function (username, callback) {
        $http.get(baseUrl + "/restClient/getAccountNameList", {params: {userName: username}})
            .success(function (data) {
                callback(data);
            });
    };

    this.getAccountBalance = function (accountNumber, callback) {
        $http.get(baseUrl + "/restClient/getAccountBalance", {params: {accountNumber: accountNumber}})
            .success(function (data) {
                callback(data);
            });
    };

    this.getAccountHistory = function (accountNumber, callback) {
        $http.get(baseUrl + "/restClient/getAccountHistory", {params: {accountNumber: accountNumber}})
            .success(function (data) {
                callback(data);
            });
    };

    this.loginPostCall = function (username, password,  callback) {
        $http({
            url: baseUrl + "/api/login",
            method: "POST",
            headers: {'Content-Type': 'application/json'},
            data: { username: username, 'password': password }
        }).success(function (response) {
                if (response.username) {
                    toastr.success("Welcome to Ignite Bank.");
                    localStorage.setItem('authToken', response.access_token);
                    localStorage.setItem('username', response.username);
                    localStorage.setItem('userRole', response.roles[0]);
                    $rootScope.currentUserName = response.username;
                    var path = "/";
                    if(response.roles[0] != "TELLER_ROLE"){
                        var welcomePage = localStorage.getItem("welcomePage");
                        path = welcomePage ? welcomePage : "/";
                    }
                    $location.path(path);
                } else {
                    toastr.error("Sorry, we were not able to find a user with that username and password.");
                    $location.path("/login");
                }
            }).
            error(function(data) {
                toastr.error("Sorry, we were not able to find a user with that username and password.");
            });
    };

});