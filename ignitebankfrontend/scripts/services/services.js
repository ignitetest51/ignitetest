
var igniteService = angular.module('igniteService', []);

igniteService.service('TellerService', function ($http, $rootScope, $location) {
    var self = this;

    var baseUrl = $('#appUrl').val();

    this.loadClientList = function (callback) {
        $http.get(baseUrl + "/restTeller/list", {}).success(function (data) {
            callback(data);
            });
    };

    this.checkCurrentUser = function (authToken, callback) {
        $http.get(baseUrl + "/restAuth/checkCurrentUser", {params: {token: authToken}}).success(function (data, textStatus, XMLHttpRequest) {
            callback(data);
        }).
        error(function(XMLHttpRequest, textStatus, errorThrown) {
                self.handleWhenUserNotLoggedIn();
        });
    };


    this.handleWhenUserNotLoggedIn = function () {
        localStorage.removeItem('authToken');
        delete $rootScope.currentUserName;
        localStorage.removeItem('username');
        localStorage.removeItem('userRole');
        $location.path("/login");
    };

    this.handleWhenUserIsLoggedIn = function (response) {
        var userRole = localStorage.getItem("userRole");
        localStorage.removeItem("username");
        localStorage.setItem("username", response.username);
        $rootScope.currentUserName = localStorage.getItem("username");
        if(response.username && $location.path() == "/"){
            if(userRole == "TELLER_ROLE"){
                $location.path("/admin");
            }else{
                $location.path("/");
            }
        }
    };

});