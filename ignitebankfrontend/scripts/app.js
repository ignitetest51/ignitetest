

var bankApp = angular.module('mainApp', ['ngRoute', 'authApp', 'toastr', 'clientApp', 'igniteService', 'directiveApp', 'tellerApp', 'clientService', 'customFilter', 'ngCookies']);


bankApp.config(function ($routeProvider) {
    var baseUrl = $('#appUrl').val();
    $routeProvider
        .when('/', {
            templateUrl: "views/client/welcome.html",
            controller: 'welcomePage'
        })
        .when('/accountSummary', {
            templateUrl: "views/client/accountSummary.html",
            controller: 'accountList'
        })
        .when('/fundTransfer', {
            templateUrl: "views/client/fundTransfer.html",
            controller: 'fundTransferController'
        })
        .when('/transferHistory', {
            templateUrl: "views/client/transferHistory.html",
            controller: 'transferHistoryController'
        })
        .when('/confirmTransfer', {
            templateUrl: "views/client/confirmTransfer.html",
            controller: 'confirmTransferController'
        })
        .when('/admin', {
            templateUrl: "views/teller/listClient.html",
            controller: 'listController'
        })
        .when('/addClient', {
            templateUrl: "views/teller/addClient.html",
            controller: 'addClientController'
        })
        .when('/addAccount/:userId', {
            templateUrl: "views/teller/addAccount.html",
            controller: 'addAccountController'
        })
        .when('/login', {
            templateUrl: "views/auth/login.html",
            controller: 'loginController',
            data: {
                public: true
            }
        })
        .when('/logout', {
            templateUrl: "views/auth/login.html",
            controller: 'logoutController',
            data: {
                public: true
            }
        })
        .when('/forgotPassword/:token', {
            templateUrl: "views/auth/forgotPassword.html",
            controller: 'forgotPassword',
            data: {
                public: true
            }
        })
        .otherwise({
            redirectTo: '/'
        });
});


bankApp.run(function ($rootScope, $location, TellerService) {
    var baseUrl = $('#appUrl').val();
//    localStorage.removeItem('welcomePage');

    $rootScope.$on("$routeChangeStart", function (event, next, prev) {
        if (next.data == null || next.data.public == null || !next.data.public) {
            var authToken = localStorage.getItem("authToken");
            if (!authToken || authToken == "") {
                TellerService.handleWhenUserNotLoggedIn();
            } else {
                TellerService.checkCurrentUser(localStorage.getItem("authToken"), function (response) {
                    if (response.result == "ERROR") {
                        TellerService.handleWhenUserNotLoggedIn();
                    } else {
                        TellerService.handleWhenUserIsLoggedIn(response);
                    }
                });
            }
        }
    });
});

