## How to run the grails application in linux: ##
1. Download the grails-2.3.3.zip .
2. Open the terminal. Unzip the grails-2.3.3.zip file in /opt folder and create a soft link for the grails by following. command: sudo ln -s grails-2.3.3/ grails
2. Create a database in mysql with name ignite
3. Go to the project folder which is ignitetest  and enter command grails run-app
4. After running the grails application open the index.html file which is in ignitebankfrontend folder(I have attached the screenshot1 in the mail which you can see how you can run the the index.html in browser)



## How to run grails unit test cases: ##
1. Go to the project folder which is ignitetest
2. run the command grails test-app -unit


## How to run grails integration test cases: ##
1. Go to the project folder which is ignitetest
2. run the command grails test-app -integration



## How to run javascript test cases: ##
For this i have used Protactor test framework for AngularJS.

1. Use npm to install Protractor globally with: npm install -g protractor

This will install two command line tools, protractor and webdriver-manager. Try running protractor --version to make sure it's working.

The webdriver-manager is a helper tool to easily get an instance of a Selenium Server running.

2. Use it to download the necessary binaries with: webdriver-manager update
3. Now start up a server with: webdriver-manager start
4. Run the grails application in new terminal.
5. Open a new terminal. Go to the project folder which is ignitetest and run the test cases with: protractor ignitebankfrontend/scripts/tests/conf.js

**Note**: For setup of the Protactor you can use this link: http://angular.github.io/protractor/#/